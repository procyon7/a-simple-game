//App.js contains the main app logic and brings everything together.
import React from 'react';
import { StyleSheet, Text, View, BackHandler} from 'react-native';
import { Ionicons, FontAwesome, Entypo } from '@expo/vector-icons';
import {AppRegistry, Button, Alert} from 'react-native'
import Header from './components/Header';
import Score from './components/Score';
import Card from './components/Card';
import { createStackNavigator } from 'react-navigation';
import helpers from './helpers';

//var Game1 = require('./Game1.js')
import Game1 from './Game1.js';

class Home extends React.Component{
    render(){
        return(
            <View style={styles.homeScreenStyle }>
                <Text style={{color: '#1264B8',
                    fontWeight: 'bold',
                    fontSize: 30,
                    textAlign:'center',
                    lineHeight: 30,
                    backgroundColor: '#6BC1F0'
                }}>
                    Welcome to Memory Game!
                </Text>
                <View>
                    <Button title={'Go to game 1'} color={'#37370E'} onPress={() => this.props.navigation.navigate('game1')} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff'
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    },
    body: {
        flex: 18,
        justifyContent: 'space-between',
        padding: 10,
        marginTop: 20
    },
    homeScreenStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFE6A7'
    }
});

export default createStackNavigator({
    home: Home,
    game1: Game1
});